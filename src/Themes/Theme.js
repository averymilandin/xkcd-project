import { createMuiTheme } from "@material-ui/core/styles";
import green from "@material-ui/core/colors/green";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#0a2b63",
    },
    secondary: {
      main: green[500],
    },
    height: "100%",
  },
});

export default theme;
