import React, { Component } from "react";
import DateSearch from "./Components/DateSearch/DateSearch";
import PrevRandomNext from "./Components/PrevRandomNext/PrevRandomNext";
import PageTitle from "./Components/PageTitle/pageTitle";
import { makeStyles } from "@material-ui/core/styles";
import axios from "axios";
import classes from "./App.module.css";

class App extends Component {
  constructor() {
    super();
    this.state = {
      curDate: new Date(),
      dateToday: new Date(),
      JSONData: null,
      maxComicNum: null,
    };
    axios
      .get("https://cors-anywhere.herokuapp.com/http://xkcd.com/info.0.json")
      .then((response) => {
        this.setState({
          JSONData: response.data,
          maxComicNum: response.data.num,
        });
        this.unvisitedPages = [...Array(response.data.num).keys()];
      })
      .catch((error) => {});
  }

  dateChangeHandler = (date) => {
    this.setState({ curDate: new Date(date) });
  };

  nextClickedHandler = () => {
    axios
      .get(
        "https://cors-anywhere.herokuapp.com/http://xkcd.com/" +
          (this.state.JSONData.num + 1) +
          "/info.0.json"
      )
      .then((response) => {
        this.updateState(response);
      })
      .catch((error) => {});
  };

  prevClickedHandler = () => {
    axios
      .get(
        "https://cors-anywhere.herokuapp.com/http://xkcd.com/" +
          (this.state.JSONData.num - 1) +
          "/info.0.json"
      )
      .then((response) => {
        this.updateState(response);
      })
      .catch((error) => {});
  };

  randClickedHandler = () => {
    const removeIndex = Math.floor(Math.random() * this.unvisitedPages.length);
    const postVal = this.unvisitedPages[removeIndex] + 1;
    this.unvisitedPages[removeIndex] = this.unvisitedPages.pop();
    axios
      .get(
        "https://cors-anywhere.herokuapp.com/http://xkcd.com/" +
          postVal +
          "/info.0.json"
      )
      .then((response) => {
        this.updateState(response);
      })
      .catch((error) => {});
  };

  updateState = (response) => {
    this.setState({
      JSONData: response.data,
      curDate: new Date(
        response.data.year,
        response.data.month - 1,
        response.data.day
      ),
    });
  };

  useStyles = makeStyles({
    headerStyle: {
      fontFamily: "Poppins",
    },
  });

  searchClickedHandler = (date) => {
    axios
      .get(
        "https://cors-anywhere.herokuapp.com/http://xkcd.com/" +
          this.dateToNum(date) +
          "/info.0.json"
      )
      .then((response) => {
        this.setState({
          JSONData: response.data,
        });
        if (parseInt(response.data.day) !== date.getDate()) {
          alert(
            "No post found for " +
              (date.getMonth() + 1) +
              "/" +
              date.getDate() +
              ". Showing post for " +
              response.data.month +
              "/" +
              response.data.day +
              " instead."
          );
        }
      })
      .catch((error) => {});
  };

  dateToNum = (date) => {
    let num = Math.floor(
      this.state.maxComicNum -
        (((this.state.dateToday.getTime() - date.getTime()) / 86400000) * 3) /
          7 +
        1
    );
    if (
      (date.getFullYear() === 2016 && date.getMonth() <= 2) ||
      date.getFullYear() < 2016
    ) {
      num += 1;
    }
    return num;
  };

  render() {
    let image = null;
    if (this.state.JSONData) {
      image = (
        <img src={this.state.JSONData.img} alt="" height="70%" width="70%" />
      );
    }
    return (
      <div className={classes.App}>
        <PageTitle useStyles={this.useStyles} />
        <DateSearch
          curDate={this.state.curDate}
          changed={this.dateChangeHandler}
          useStyles={this.useStyles}
          searchClicked={() => this.searchClickedHandler(this.state.curDate)}
        />
        <PrevRandomNext
          nextClicked={this.nextClickedHandler}
          nextDisabled={
            this.state.JSONData
              ? this.state.maxComicNum === this.state.JSONData.num
              : true
          }
          prevDisabled={
            this.state.JSONData ? 1 === this.state.JSONData.num : true
          }
          prevClicked={this.prevClickedHandler}
          randClicked={this.randClickedHandler}
        />
        {image}
      </div>
    );
  }
}

export default App;
