import React from "react";
import { Typography } from "@material-ui/core";

const pageTitle = (props) => {
  const classes = props.useStyles();
  return (
    <Typography className={classes.headerStyle} variant="h4" color="primary">
      Welcome to the XKCD comic-lookup tool!
    </Typography>
  );
};

export default pageTitle;
