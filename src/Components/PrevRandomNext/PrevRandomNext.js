import React from "react";
import { Button } from "@material-ui/core";

const PrevRandomNext = (props) => (
  <div>
    <Button
      onClick={props.prevClicked}
      variant="outlined"
      color="primary"
      disabled={props.prevDisabled}
    >
      Prev
    </Button>
    <Button
      onClick={props.randClicked}
      variant="contained"
      color="primary"
      style={{ margin: "5px" }}
    >
      Random
    </Button>
    <Button
      onClick={props.nextClicked}
      variant="outlined"
      color="primary"
      disabled={props.nextDisabled}
    >
      Next
    </Button>
  </div>
);

export default PrevRandomNext;
