import React from "react";
import classes from "./searchBox.module.css";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";

const SearchBox = (props) => (
  <div className={classes.SearchBox}>
    <p>{props.children}</p>
    <MuiPickersUtilsProvider utils={DateFnsUtils} color="primary">
      <KeyboardDatePicker
        variant="inline"
        value={props.curDate}
        onChange={(date) => props.changed(date)}
      />
    </MuiPickersUtilsProvider>
  </div>
);

export default SearchBox;
