import React, { Fragment } from "react";
import SearchBox from "./SearchBox/SearchBox";
import { Button } from "@material-ui/core";
import classes from "./dateSearch.module.css";
import { Typography } from "@material-ui/core";

const dateSearch = (props) => (
  <Fragment>
    <Typography
      className={props.useStyles().headerStyle}
      variant="h6"
      color="primary"
    >
      Enter the date of a comic you would like to see
    </Typography>
    <div className={classes.SearchBar}>
      <SearchBox curDate={props.curDate} changed={props.changed} />
      <Button
        onClick={props.searchClicked}
        variant="contained"
        size="small"
        color="primary"
      >
        Search
      </Button>
    </div>
  </Fragment>
);

export default dateSearch;
